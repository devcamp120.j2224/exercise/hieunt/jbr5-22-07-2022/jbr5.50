package com.jbr550.customeraccountapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.jbr550.customeraccountapi.model.Account;

@Service
public class AccountService {
    private static ArrayList<Account> allAccounts = new ArrayList<>();
    static {
        Account account1 = new Account(1,
            CustomerService.getCustomer1(), 20000);
        Account account2 = new Account(2,
            CustomerService.getCustomer2(), 25000);
        Account account3 = new Account(3,
            CustomerService.getCustomer3(), 30000);
        allAccounts.add(account1);
        allAccounts.add(account2);
        allAccounts.add(account3);
    }
    public static ArrayList<Account> getAllAccounts() {
        return allAccounts;
    }
}
