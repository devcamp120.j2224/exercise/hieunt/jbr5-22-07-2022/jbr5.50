package com.jbr550.customeraccountapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.jbr550.customeraccountapi.model.Customer;

@Service
public class CustomerService {
    private static ArrayList<Customer> allCustomers = new ArrayList<>();
    private static Customer customer1 = new Customer(1, "Lan", 15);
    private static Customer customer2 = new Customer(2, "Hoa", 20);
    private static Customer customer3 = new Customer(3, "Nam", 10);
    static {
        allCustomers.add(customer1);
        allCustomers.add(customer2);
        allCustomers.add(customer3);
    }
    public static ArrayList<Customer> getAllCustomers() {
        return allCustomers;
    }
    public static Customer getCustomer1() {
        return customer1;
    }
    public static void setCustomer1(Customer customer1) {
        CustomerService.customer1 = customer1;
    }
    public static Customer getCustomer2() {
        return customer2;
    }
    public static void setCustomer2(Customer customer2) {
        CustomerService.customer2 = customer2;
    }
    public static Customer getCustomer3() {
        return customer3;
    }
    public static void setCustomer3(Customer customer3) {
        CustomerService.customer3 = customer3;
    }
}
