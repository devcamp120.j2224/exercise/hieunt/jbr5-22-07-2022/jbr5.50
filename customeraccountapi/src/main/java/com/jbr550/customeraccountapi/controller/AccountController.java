package com.jbr550.customeraccountapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.jbr550.customeraccountapi.service.AccountService;
import com.jbr550.customeraccountapi.model.Account;

@RestController
@CrossOrigin
public class AccountController {
    @GetMapping("/accounts")
    public ArrayList<Account> getAllAccounts() {
        ArrayList<Account> allAccounts = AccountService.getAllAccounts();
        return allAccounts;
    }
}
