package com.jbr550.customeraccountapi.controller;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.jbr550.customeraccountapi.service.CustomerService;
import com.jbr550.customeraccountapi.model.Customer;

@RestController
@CrossOrigin
public class CustomerController {
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> allCustomers = CustomerService.getAllCustomers();
        return allCustomers;
    }
}
